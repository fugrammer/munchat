package com.example.thamt.munchat;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.ArgumentMarshaller;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBDeleteExpression;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBQueryExpression;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBScanExpression;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.PaginatedQueryList;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.PaginatedScanList;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidentity.AmazonCognitoIdentity;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thamt on 11/10/2016.
 */

public class DatabaseBackground extends Thread {

    private static final String TAG = "Debug" ;

    public Handler getmHandler() {
        return mHandler;
    }

    public Handler mHandler;

    public Handler getdHandler() {
        return dHandler;
    }

    public Handler dHandler;

    static long lastTime=0;

    private void writeData2(){
        ArrayList<DataMap> dataMaps = new ArrayList<DataMap>();
        DataMap data = new DataMap();
        data.setUserID("Thomas");
        Condition rangeKeyCondition = new Condition().
                withComparisonOperator(ComparisonOperator.GT)
                .withAttributeValueList(new AttributeValue().withN(String.valueOf(lastTime)));

        DynamoDBQueryExpression queryExpression = new DynamoDBQueryExpression()
                .withHashKeyValues(data)
                .withRangeKeyCondition("Time", rangeKeyCondition)
                .withConsistentRead(false);
        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":val1", new AttributeValue().withS("Time"));
        eav.put(":val2", new AttributeValue().withN(String.valueOf(lastTime)));
        try {
            PaginatedQueryList<DataMap> result = mapper.query(DataMap.class, queryExpression);
            for (DataMap record: result
                    ) {
                if (record.getTime()>lastTime){
                    dataMaps.add(record);
                    lastTime = record.getTime();
                }
            }
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putLong("lastTime",DatabaseBackground.lastTime);
            editor.commit();

            if (!dataMaps.isEmpty()) {
                Message m = new Message();
                m.obj = dataMaps;
                pHandler.sendMessage(m);
            }
        } catch (Exception e){
            Log.e("AWS Error", e.toString());
            return;
        }

    }
    @Override
    public void run(){

        Looper.prepare();
        mHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.arg1==1) writeData(msg);
                else deleteData(msg);
            }
        };

        dHandler = new Handler() {
            public void handleMessage(Message msg) {
                writeData2();
            }
        };

        Looper.loop();

    }

    private void writeData(Message msg){
        Record record = (Record) msg.obj;
        DataMap data = new DataMap();
        data.setUserID("Thomas");
        data.setTime(System.currentTimeMillis()/1000);
        data.setGenerousity(record.getGenerousity());
        data.setPrice(record.getPrice());
        data.setQuality(record.getQuality());
        data.setLocation(record.getLocation());
        data.setComment(record.getComment());
        data.setImage(record.getImage());
        data.setUploader(record.getUpLoader());
        data.setLat(record.getLat());
        data.setLon(record.getLon());
        mapper.save(data);
    }

    private void deleteData(Message msg){

        long time = (long) msg.obj;
        DataMap data = new DataMap();
        data.setUserID("Thomas");

        Condition rangeKeyCondition = new Condition().
                withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withN(String.valueOf(time)));

        DynamoDBQueryExpression queryExpression = new DynamoDBQueryExpression()
                .withHashKeyValues(data)
                .withRangeKeyCondition("Time", rangeKeyCondition)
                .withConsistentRead(false);

        PaginatedQueryList<DataMap> result = mapper.query(DataMap.class, queryExpression);
        for (DataMap rec: result){
            mapper.delete(rec);
        }
    }

    public interface DataChangeListener
    {
        public void onDataChanged(String data);
    }

    DataChangeListener mListener;
    DynamoDBMapper mapper;
    AmazonDynamoDBClient ddbClient;
    Handler pHandler;
    SharedPreferences sharedPref;
    Context context;

    public DatabaseBackground(Activity context, DataChangeListener dataChangeListener, DynamoDBMapper mapper, AmazonDynamoDBClient ddbClient, Handler pHandler){
        this.mListener = dataChangeListener;
        this.mapper = mapper;
        this.ddbClient = ddbClient;
        this.pHandler = pHandler;
        this.context = context;
        this.mapper = new DynamoDBMapper(ddbClient);
        ddbClient.setRegion(Region.getRegion(Regions.US_WEST_2));
        sharedPref = context.getPreferences(context.MODE_PRIVATE);
        long lastTime = sharedPref.getLong("lastTime",0);
        DatabaseBackground.lastTime = lastTime;
    }
}
