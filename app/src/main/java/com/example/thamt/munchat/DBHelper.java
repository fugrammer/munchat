package com.example.thamt.munchat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import static android.R.attr.id;

/**
 * Created by thamt on 6/10/2016.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";
    public static final String TABLE_NAME = "records";
    public static final String COLUMN_IMAGE_PATH = "imagePath";
    public static final String COLUMN_IMAGE_COMMENT = "imageComment";
    public static final String COLUMN_IMAGE_RatingP = "imageP";
    public static final String COLUMN_IMAGE_RatingQ = "imageQ";
    public static final String COLUMN_IMAGE_RatingG = "imageG";
    public static final String COLUMN_IMAGE_LOCATION = "imageLocation";
    public static final String COLUMN_IMAGE_UPLOADER = "imageUploader";
    public static final String COLUMN_IMAGE_LAT = "imageLat";
    public static final String COLUMN_IMAGE_LON = "imageLon";
    public static final String COLUMN_IMAGE_Fav = "imageFav";
    public static final String COLUMN_IMAGE_TIME = "imageTime";
    private HashMap hp;

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table records " +
                        "(imagePath text,imageComment text,imageP float, imageQ float,imageG text, imageLocation text, imageUploader text, imageLat double, imageLon double, imageFav text, imageTime long)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(db);
    }

    public boolean insertRecord  (String name, String comment,float ratingQ, float ratingP, float ratingG)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_IMAGE_PATH, name);
        contentValues.put(COLUMN_IMAGE_COMMENT,comment);
        contentValues.put(COLUMN_IMAGE_RatingQ, ratingQ);
        contentValues.put(COLUMN_IMAGE_RatingP, ratingP);
        contentValues.put(COLUMN_IMAGE_RatingG, ratingG);
        contentValues.put(COLUMN_IMAGE_LOCATION,"");
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertRecord  (String name, String comment,float ratingQ, float ratingP, float ratingG,String location)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_IMAGE_PATH, name);
        contentValues.put(COLUMN_IMAGE_COMMENT,comment);
        contentValues.put(COLUMN_IMAGE_RatingQ, ratingQ);
        contentValues.put(COLUMN_IMAGE_RatingP, ratingP);
        contentValues.put(COLUMN_IMAGE_RatingG, ratingG);
        contentValues.put(COLUMN_IMAGE_LOCATION,location);
        contentValues.put(COLUMN_IMAGE_LAT,0);
        contentValues.put(COLUMN_IMAGE_LON,0);
        contentValues.put(COLUMN_IMAGE_Fav,"0");
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertRecord  (String name, String comment,float ratingQ, float ratingP, float ratingG,String location,String uploader)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_IMAGE_PATH, name);
        contentValues.put(COLUMN_IMAGE_COMMENT,comment);
        contentValues.put(COLUMN_IMAGE_RatingQ, ratingQ);
        contentValues.put(COLUMN_IMAGE_RatingP, ratingP);
        contentValues.put(COLUMN_IMAGE_RatingG, ratingG);
        contentValues.put(COLUMN_IMAGE_LOCATION,location);
        contentValues.put(COLUMN_IMAGE_UPLOADER,uploader);
        contentValues.put(COLUMN_IMAGE_LAT,0);
        contentValues.put(COLUMN_IMAGE_LON,0);
        contentValues.put(COLUMN_IMAGE_Fav,"0");
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertRecord  (String name, String comment,float ratingQ, float ratingP, float ratingG,String location,String uploader, double lat, double lon)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_IMAGE_PATH, name);
        contentValues.put(COLUMN_IMAGE_COMMENT,comment);
        contentValues.put(COLUMN_IMAGE_RatingQ, ratingQ);
        contentValues.put(COLUMN_IMAGE_RatingP, ratingP);
        contentValues.put(COLUMN_IMAGE_RatingG, ratingG);
        contentValues.put(COLUMN_IMAGE_LOCATION,location);
        contentValues.put(COLUMN_IMAGE_UPLOADER,uploader);
        contentValues.put(COLUMN_IMAGE_LAT,lat);
        contentValues.put(COLUMN_IMAGE_LON,lon);
        contentValues.put(COLUMN_IMAGE_Fav,"0");
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }
    public boolean insertRecord  (long time,String name, String comment,float ratingQ, float ratingP, float ratingG,String location,String uploader, double lat, double lon)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_IMAGE_PATH, name);
        contentValues.put(COLUMN_IMAGE_COMMENT,comment);
        contentValues.put(COLUMN_IMAGE_RatingQ, ratingQ);
        contentValues.put(COLUMN_IMAGE_RatingP, ratingP);
        contentValues.put(COLUMN_IMAGE_RatingG, ratingG);
        contentValues.put(COLUMN_IMAGE_LOCATION,location);
        contentValues.put(COLUMN_IMAGE_UPLOADER,uploader);
        contentValues.put(COLUMN_IMAGE_LAT,lat);
        contentValues.put(COLUMN_IMAGE_LON,lon);
        contentValues.put(COLUMN_IMAGE_Fav,"0");
        contentValues.put(COLUMN_IMAGE_TIME,time);
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }
    public Cursor getData(String name){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+ TABLE_NAME + " where "+COLUMN_IMAGE_PATH+"= \""+name+"\"", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        return numRows;
    }

    public boolean updateRecord (String name, String comment,float ratingQ, float ratingP, float ratingG,String location,String uploader, double lat, double lon,String fav)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_IMAGE_PATH, name);
        contentValues.put(COLUMN_IMAGE_COMMENT,comment);
        contentValues.put(COLUMN_IMAGE_RatingQ, ratingQ);
        contentValues.put(COLUMN_IMAGE_RatingP, ratingP);
        contentValues.put(COLUMN_IMAGE_RatingG, ratingG);
        contentValues.put(COLUMN_IMAGE_LOCATION,location);
        contentValues.put(COLUMN_IMAGE_UPLOADER,uploader);
        contentValues.put(COLUMN_IMAGE_LAT,lat);
        contentValues.put(COLUMN_IMAGE_LON,lon);
        contentValues.put(COLUMN_IMAGE_Fav,fav);
        Log.e("updateRecord ", fav);
        db.update(TABLE_NAME, contentValues, COLUMN_IMAGE_PATH+ " = ? ", new String[] { name } );
        return true;
    }

    public Integer deleteRecord (String name)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME,COLUMN_IMAGE_PATH + " = ? ", new String[] {name });
    }

    public ArrayList<String> getAllImages()
    {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from " +TABLE_NAME, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex(COLUMN_IMAGE_PATH)));
            res.moveToNext();
        }
        return array_list;
    }
}