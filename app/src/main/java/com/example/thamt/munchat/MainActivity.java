package com.example.thamt.munchat;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.*;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.*;
import com.amazonaws.services.dynamodbv2.model.*;
import com.google.android.gms.vision.Frame;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity
        implements FriendFragment.OnFragmentInteractionListener,DatabaseBackground.DataChangeListener,
        PhotoShow.OnFragmentInteractionListener,PhotoPreview.OnFragmentInteractionListener,
        ProfileFragment.OnFragmentInteractionListener, NavigationView.OnNavigationItemSelectedListener,NewsFragment.OnFragmentInteractionListener {

    private static final int START_CAMERA_ACTIVITY = 2;
    static String MyID="-1";
    static final int REQUEST_IMAGE_CAPTURE = 1;
    String currentPhotoPath;
    String[] perms = {"android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.ACCESS_FINE_LOCATION","android.permission.INTERNET"};
    int permsRequestCode = 200;
    AmazonDynamoDBClient ddbClient;
    DynamoDBMapper mapper;
    public Handler mHandler;
    public Handler pHandler;
    public Handler dHandler;
    static boolean started = true;
    static public Deque<String> prevTitle;
    DatabaseBackground myThread;

    @Override
    protected void onStart(){
        super.onStart();
        started = true;
    }

    @Override
    protected void onStop(){
        super.onStop();
        started = false;
    }

    @Override
    public void onDataChanged(String data) {
    }

    @Override
    public void onBackPressed(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }
        //super.onBackPressed();
        //TextView textView = (TextView) findViewById(R.id.titleTextView);
        //textView.setText(prevTitle.pollLast());

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = (Fragment) fragmentManager.findFragmentById(R.id.MainFrame);

        if (fragment instanceof NewsFragment){
            finish();
        }else if (fragment instanceof PhotoShow){super.onBackPressed();}

            else
        {
            changeFragment("newsfeed");
        }
//        if (fragment instanceof ProfileFragment){
//            Spinner spinner = (Spinner) findViewById(R.id.spinner2);
//            spinner.setVisibility(View.VISIBLE);
//        }else if (fragment==null){
//            Log.e("finish", "onBackPressed: " );
//            finish();
//        }else if (fragment instanceof PhotoPreview){
//            FragmentManager fm = getSupportFragmentManager();
//            for (int i=0;i<count-1;i++) fm.popBackStackImmediate();
//            count=1;
//        }
//        else{
//            Spinner spinner = (Spinner) findViewById(R.id.spinner2);
//            spinner.setVisibility(View.INVISIBLE);
//        }
    }
    private void getNewID(){
        final SharedPreferences sharedPref = this.getPreferences(this.MODE_PRIVATE);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter UserID");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MyID = input.getText().toString();
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                TextView tv = (TextView) navigationView.findViewById(R.id.userID);
                tv.setText(MyID);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("MyID",MyID);
                editor.commit();
                changeFragment("profile",MyID);
            }
        });
        builder.show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activityInstance = this;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(perms, permsRequestCode);
        }

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        prevTitle = new ArrayDeque<>();

        //Get UserID
        final SharedPreferences sharedPref = this.getPreferences(this.MODE_PRIVATE);
        String id = sharedPref.getString("MyID","-1");
        if (id=="-1"){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Enter UserID");
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MyID = input.getText().toString();
                    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                    TextView tv = (TextView) navigationView.findViewById(R.id.userID);
                    tv.setText(MyID);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("MyID",MyID);
                    editor.commit();
                }
            });
            builder.show();
        }else{
            MyID = id;
        }
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "us-west-2:4266ed45-1269-476d-b6d4-0e845ad547aa", // Identity Pool ID
                Regions.US_WEST_2 // Region
        );

        // Receive updates from DatabaseBackground
        mHandler = new Handler() {
            public void handleMessage(Message msg) {
                ArrayList<DataMap> dataMaps = (ArrayList<DataMap>) msg.obj;
                int i=0;
                if (!dataMaps.isEmpty()) {
                    DBHelper mydb = new DBHelper(getBaseContext());
                    for (DataMap record : dataMaps) {
                        if (record.getUploader() == MainActivity.MyID) continue;
                        Bitmap bitmap = ImageHelper.decodeBase64(record.getImage());
                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()).concat(String.valueOf(i++));
                        String imageFileName = timeStamp + ".jpg";
                        File storageDir = Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES);
                        String path;
                        path = storageDir.getAbsolutePath() + "/" + imageFileName;
                        try {
                            FileOutputStream out = new FileOutputStream(path);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                        } catch (Exception e) {
                        }
                        mydb.insertRecord(record.getTime(),path, record.getComment(), record.getQuality(), record.getPrice(), record.getGenerousity(), record.getLocation(),record.getUploader(),record.getLat(),record.getLon());
                    }
                    mydb.close();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    Fragment currentFragment = (Fragment) fragmentManager.findFragmentById(R.id.MainFrame);
                    if (currentFragment instanceof UpdatableFragment){
                        ((UpdatableFragment) currentFragment).update();
                    }

                }
            }
        };
        ddbClient = new AmazonDynamoDBClient(credentialsProvider);
        myThread = new DatabaseBackground(this,this,mapper,ddbClient,mHandler);
        myThread.start();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                pHandler = myThread.getmHandler();
                dHandler = myThread.getdHandler();
                //mDatabase.run();
                TextView tv = (TextView) navigationView.findViewById(R.id.userID);
                tv.setText(MyID);
                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getNewID();
                       }
                });
            }
        }, 1000);
        final Handler handler3 = new Handler();
        handler3.postDelayed(new Runnable() {
            @Override
            public void run() {
                FragmentManager fragmentManager = getSupportFragmentManager();
                Fragment currentFragment = (Fragment) fragmentManager.findFragmentById(R.id.MainFrame);
                if (currentFragment instanceof UpdatableFragment){
                    //((UpdatableFragment) currentFragment).update();
                }
            }
        }, 500);
        final Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                mDatabase.run();
            }
        }, 30000);
        changeFragment("newsfeed");
    }

    Runnable mDatabase = new Runnable() {
        @Override
        public void run() {
            try {
               dHandler.sendEmptyMessage(0);
            } finally {
                if (started == true) mHandler.postDelayed(mDatabase, 20000);
            }
        }
    };

    /** Fragment management **/

    private void changeFragment(String fragment, String... values) {
        if (fragment == "profile") {
            ProfileFragment profileFragment = new ProfileFragment();
            Bundle args = new Bundle();
            args.putString("userID",values[0]);
            profileFragment.setArguments(args);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFrame, profileFragment,values[0]);
            transaction.addToBackStack("1");
            transaction.commitAllowingStateLoss();
            prevTitle.add(textView.getText().toString());
            textView.setText(values[0]);
        }
        else if (fragment == "showPhoto"){
            PhotoShow photoFragment = new PhotoShow();
            Bundle args = new Bundle();
            ArrayList<String> list = new ArrayList<String>();
            for (String s : values) {
                list.add(s);
            };
            args.putStringArrayList("imageDetails",list);
            photoFragment.setArguments(args);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFrame, photoFragment);
            transaction.addToBackStack("1");
            transaction.commitAllowingStateLoss();
        }
        else if (fragment == "friend"){
            FriendFragment friendFragment = new FriendFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFrame, friendFragment);
            transaction.addToBackStack("1");
            transaction.commitAllowingStateLoss();
            TextView textView = (TextView) findViewById(R.id.titleTextView);
            prevTitle.add(textView.getText().toString());
            textView.setText("Friends");
        }else if (fragment == "newsfeed"){
            NewsFragment newsFragment = new NewsFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFrame, newsFragment);
            transaction.addToBackStack("1");
            transaction.commitAllowingStateLoss();
            TextView textView = (TextView) findViewById(R.id.titleTextView);
            prevTitle.add(textView.getText().toString());
            textView.setText("MunchAt");
            Spinner spinner = (Spinner) findViewById(R.id.spinner2);
            spinner.setVisibility(View.INVISIBLE);
        }
    }
    static Activity activityInstance;

    public static Activity getInstance() {
        return activityInstance;
    }
    @Override
    public void onFragmentInteraction(String path, String comment ,String quality, String price, String generousity,String location,String uploader, String lat, String lon,String fav){
        this.changeFragment("showPhoto",path,comment,quality,price,generousity,location,uploader,lat,lon,fav);
    }
    @Override
    public void onFragmentInteraction(String uploader){
        this.changeFragment("profile",uploader);
    }

    @Override
    public void onFavourite(String name, String comment,float ratingQ, float ratingP, float ratingG,String location,String uploader, double lat, double lon,String fav){
        DBHelper mydb = new DBHelper(getBaseContext());
        mydb.updateRecord(name,comment,ratingQ,ratingP,ratingG,location,uploader,lat,lon,fav);
        mydb.close();
    };

    @Override
    public void onDelete(String name, String comment,float ratingQ, float ratingP, float ratingG,String location,String uploader, double lat, double lon){
        DBHelper mydb = new DBHelper(getBaseContext());
        Cursor cur = mydb.getData(name);
        cur.moveToFirst();
        long time = cur.getLong(cur.getColumnIndex(mydb.COLUMN_IMAGE_TIME));
        mydb.deleteRecord(name);
        mydb.close();
        Message m = new Message();
        m.obj = time;
        m.arg1 = 0;
        pHandler.sendMessage(m);
        super.onBackPressed();

    }
    /**Below: Camera, Nav, Toolbar**/

    // After capturing image with camera
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            PhotoPreview previewFragment = new PhotoPreview();
            Bundle args = new Bundle();
            args.putString("imagePath",currentPhotoPath);
            previewFragment.setArguments(args);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFrame, previewFragment);

            transaction.addToBackStack("1");
            transaction.commitAllowingStateLoss();
            TextView textView = (TextView) findViewById(R.id.titleTextView);
            prevTitle.add(textView.getText().toString());
            textView.setText("Preview");
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setVisibility(View.INVISIBLE);
        }else if (requestCode==START_CAMERA_ACTIVITY && resultCode ==  RESULT_OK){
            //Toast.makeText(this,data.getStringExtra("imagePath") , Toast.LENGTH_SHORT).show();
            PhotoPreview previewFragment = new PhotoPreview();
            Bundle args = new Bundle();
            currentPhotoPath=data.getStringExtra("imagePath");
            args.putString("imagePath",data.getStringExtra("imagePath"));
            previewFragment.setArguments(args);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFrame, previewFragment);
            transaction.addToBackStack("1");
            transaction.commitAllowingStateLoss();
            TextView textView = (TextView) findViewById(R.id.titleTextView);
            prevTitle.add(textView.getText().toString());
            textView.setText("Preview");
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setVisibility(View.INVISIBLE);
        }
    }

    @Override

    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        switch(permsRequestCode){

            case 200:

                boolean cameraAccepted = grantResults[0]==PackageManager.PERMISSION_GRANTED;

                break;

        }

    }
    private boolean canMakeSmores(){

        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);

    }
    Fragment prevFragment;
    Uri photoURI;
    public void cameraActivated(View view){
        FragmentManager fragmentManager = getSupportFragmentManager();
        prevFragment = (Fragment) fragmentManager.findFragmentById(R.id.MainFrame);
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = timeStamp + ".jpg";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        currentPhotoPath = storageDir.getAbsolutePath() + "/" + imageFileName;
        File file = new File(currentPhotoPath);
        Uri outputFileUri = Uri.fromFile(file);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        takePictureIntent.putExtra(MediaStore.ACTION_IMAGE_CAPTURE,outputFileUri);
        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        /** Work in progress **/
//        Intent intent = new Intent(this,CameraActivity.class);
//        startActivityForResult(intent,START_CAMERA_ACTIVITY);
    }

    static int count =1;
    boolean More;
    @Override
    public void onMunched(String comment ,float quality, float price, float generousity,String location,double lat,double lon,boolean more){
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        More = more;
        if (more == false){
            changeFragment("profile",MyID);
            count=1;
            fab.setVisibility(View.VISIBLE);
        }
        String image = ImageHelper.encodeToBase64(ImageHelper.decodeSampledBitmapFromResource(currentPhotoPath,200,200), Bitmap.CompressFormat.JPEG,100);
        Record record = new Record(image,comment, price,quality,generousity,location,MainActivity.MyID,lat,lon);
        Message m = new Message();
        m.obj = record;
        m.arg1 = 1;
        pHandler.sendMessage(m);
        final Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                dHandler.sendEmptyMessage(0);
            }
        }, 500);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                FragmentManager fragmentManager = getSupportFragmentManager();
                Fragment currentFragment = (Fragment) fragmentManager.findFragmentById(R.id.MainFrame);
                if (currentFragment instanceof UpdatableFragment){
                    ((UpdatableFragment) currentFragment).update();
                }
            }
        }, 1000);
        if (more){
            count++;
            fab.performClick();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        final int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (id == R.id.nav_profile) {
                    changeFragment("profile",MyID);
                } else if (id == R.id.nav_friends) {
                    changeFragment("friend");
                } else if (id == R.id.nav_home){
                    changeFragment("newsfeed");
                }
            }
        }, 300);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
