package com.example.thamt.munchat;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.media.Rating;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;
import static android.content.Context.LOCATION_SERVICE;
import static com.example.thamt.munchat.AppConfig.FOOD_ID;
import static com.example.thamt.munchat.AppConfig.GEOMETRY;
import static com.example.thamt.munchat.AppConfig.GOOGLE_BROWSER_API_KEY;
import static com.example.thamt.munchat.AppConfig.ICON;
import static com.example.thamt.munchat.AppConfig.LATITUDE;
import static com.example.thamt.munchat.AppConfig.LOCATION;
import static com.example.thamt.munchat.AppConfig.LONGITUDE;
import static com.example.thamt.munchat.AppConfig.NAME;
import static com.example.thamt.munchat.AppConfig.OK;
import static com.example.thamt.munchat.AppConfig.PLACE_ID;
import static com.example.thamt.munchat.AppConfig.PROXIMITY_RADIUS;
import static com.example.thamt.munchat.AppConfig.REFERENCE;
import static com.example.thamt.munchat.AppConfig.STATUS;
import static com.example.thamt.munchat.AppConfig.VICINITY;
import static com.example.thamt.munchat.AppConfig.ZERO_RESULTS;

public class PhotoPreview extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final int GAP = 40;
    private static final String ARG_PARAM1 = "imagePath";
    public static LocationManager locationManager;
    public static GoogleApiClient mGoogleApiClient;
    // TODO: Rename and change types of parameters
    private String mParam1;
    View view;
    private OnFragmentInteractionListener mListener;
    private Activity mActivity;
    public PhotoPreview() {
        // Required empty public constructor
    }
    private static double lat;
    private static double lon;
    private static float plateGen;
    private static float smileGen;
    private static float priceGen;

    public static PhotoPreview newInstance(String param1) {
        PhotoPreview fragment = new PhotoPreview();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString("imagePath");
        }
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        locationManager = (LocationManager) mActivity.getSystemService(LOCATION_SERVICE);
        mGoogleApiClient.connect();
    }

    ImageView mImageView;

    private void munch(boolean more){
        if (mListener != null) {
            String comment = "";
            TextView textView = (TextView) mActivity.findViewById(R.id.commentText);
            if (textView.getText() != "")
                comment = textView.getText().toString();
            //RatingBar rbQ = (RatingBar) mActivity.findViewById(R.id.ratingBarQ);
            RatingBar rbP = (RatingBar) mActivity.findViewById(R.id.ratingBarP);
            //RatingBar rbG = (RatingBar) mActivity.findViewById(R.id.ratingBarG);
            Spinner spinner = (Spinner) mActivity.findViewById(R.id.spinner);
            String location;
            try {
                location = spinner.getSelectedItem().toString();
            }catch (Exception e){
                location="";
            }
            if (location=="Other"){
                EditText editText = (EditText) view.findViewById(R.id.customLocationEditText);
                location = editText.getText().toString();
            }
            try {
                mListener.onMunched(comment, smileGen,priceGen,plateGen, location,PhotoPreview.lat,PhotoPreview.lon,more);
            }catch(Exception e){
                mListener.onMunched("", smileGen, priceGen, plateGen, location,PhotoPreview.lat,PhotoPreview.lon,more);
            }

        }
    }
    Context context;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_photo_preview, container, false);
        //context = container.getContext();
        mImageView = (ImageView) view.findViewById(R.id.imageView2);
        setPic();
        final Button munchButton = (Button) view.findViewById(R.id.munchButton);
        munchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            munch(false);
            }
        });
        Button munchButton2 = (Button) view.findViewById(R.id.moreButton);
        munchButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                munch(true);

            }
        });
        if (mGoogleApiClient.isConnected()){
            if (ActivityCompat.checkSelfPermission(mActivity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mActivity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return view;
            }
            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation!=null) {
                loadNearByPlaces(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            }
        }
        EditText textView = (EditText) view.findViewById(R.id.commentText);
        textView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        plateGen = 3;
        smileGen = 3;
        final ImageView plates = (ImageView) view.findViewById(R.id.plateImageView);
        plates.setOnTouchListener(new View.OnTouchListener() {
            private float mPrevX;
            private float mPrevY;
            private int level = 3,clevel;
            private int height = plates.getHeight();

            private void changeImage (View v,int level){
                switch (level){
                    case 0:
                        v.setBackgroundResource(R.drawable.plate_01);
                        break;
                    case 1:
                        v.setBackgroundResource(R.drawable.plate_02);
                        break;
                    case 2:
                        v.setBackgroundResource(R.drawable.plate_03);
                        break;
                    case 3:
                        v.setBackgroundResource(R.drawable.plate_04);
                        break;
                    case 4:
                        v.setBackgroundResource(R.drawable.plate_05);
                        break;
                    case 5:
                        v.setBackgroundResource(R.drawable.plate_06);
                        break;
                }
            }
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float currY;
                int action = event.getAction();
                height = v.getHeight();
                Log.e(TAG, "onTouch: " + String.valueOf(level));
                Log.e(TAG, "onTouch: " + String.valueOf(clevel));
                switch (action) {
                    case MotionEvent.ACTION_DOWN: {
                        mPrevY = event.getRawY();
                    }
                    case MotionEvent.ACTION_MOVE:
                    {
                        currY = event.getRawY();
                        clevel =(int)(mPrevY-currY)/GAP;
                        switch (clevel){
                        case -5:
                            changeImage(v,level-5);
                            break;
                        case -4:
                            changeImage(v,level-4);
                            break;
                        case -3:
                            changeImage(v,level-3);
                            break;
                        case -2:
                            changeImage(v,level-2);
                            break;
                        case -1:
                            changeImage(v,level+-1);
                            break;
                        case 0:
                            changeImage(v,level);
                            break;
                        case 1:
                            changeImage(v,level+1);
                            break;
                        case 2:
                            changeImage(v,level+2);
                            break;
                        case 3:
                            changeImage(v,level+3);
                            break;
                        case 4:
                            changeImage(v,level+4);
                            break;
                        case 5:
                            changeImage(v,level+5);
                            break;
                        }
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL:
                        break;

                    case MotionEvent.ACTION_UP:
                        level += clevel;
                        if (level>5) level = 5;
                        if (level<0) level = 0;
                        plateGen = level;
                        break;
                }
                return true;
            }
        });
        final ImageView smiles = (ImageView) view.findViewById(R.id.smileImageView3);
        smiles.setOnTouchListener(new View.OnTouchListener() {
            private float mPrevY;
            private int level = 3,clevel;
            private void changeImage (View v,int level){
                switch (level){
                    case 1:
                        v.setBackgroundResource(R.drawable.smile_01);
                        break;
                    case 2:
                        v.setBackgroundResource(R.drawable.smile_02);
                        break;
                    case 3:
                        v.setBackgroundResource(R.drawable.smile_03);
                        break;
                    case 4:
                        v.setBackgroundResource(R.drawable.smile_04);
                        break;
                    case 5:
                        v.setBackgroundResource(R.drawable.smile_05);
                        break;
                }
            }
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float currY;
                int action = event.getAction();
                Log.e(TAG, "onTouch: " + String.valueOf(level));
                Log.e(TAG, "onTouch: " + String.valueOf(clevel));
                switch (action) {
                    case MotionEvent.ACTION_DOWN: {
                        mPrevY = event.getRawY();
                    }
                    case MotionEvent.ACTION_MOVE:
                    {
                        currY = event.getRawY();
                        clevel =-(int)(mPrevY-currY)/GAP;
                        switch (clevel){
                            case -5:
                                changeImage(v,level-5);
                                break;
                            case -4:
                                changeImage(v,level-4);
                                break;
                            case -3:
                                changeImage(v,level-3);
                                break;
                            case -2:
                                changeImage(v,level-2);
                                break;
                            case -1:
                                changeImage(v,level+-1);
                                break;
                            case 0:
                                changeImage(v,level);
                                break;
                            case 1:
                                changeImage(v,level+1);
                                break;
                            case 2:
                                changeImage(v,level+2);
                                break;
                            case 3:
                                changeImage(v,level+3);
                                break;
                            case 4:
                                changeImage(v,level+4);
                                break;
                            case 5:
                                changeImage(v,level+5);
                                break;
                        }
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL:
                        break;

                    case MotionEvent.ACTION_UP:
                        level += clevel;
                        if (level>5) level = 5;
                        if (level<1) level = 1;
                        smileGen = level;
                        break;
                }
                return true;
            }
        });

        final ImageView prices = (ImageView) view.findViewById(R.id.priceImageView);
        prices.setOnTouchListener(new View.OnTouchListener() {
            private float mPrevY;
            private int level = 3,clevel;
            private void changeImage (View v,int level){
                switch (level){
                    case 0:
                        v.setBackgroundResource(R.drawable.price_01);
                    case 1:
                        v.setBackgroundResource(R.drawable.price_02);
                        break;
                    case 2:
                        v.setBackgroundResource(R.drawable.price_03);
                        break;
                    case 3:
                        v.setBackgroundResource(R.drawable.price_04);
                        break;
                    case 4:
                        v.setBackgroundResource(R.drawable.price_05);
                        break;
                    case 5:
                        v.setBackgroundResource(R.drawable.price_06);
                        break;
                }
            }
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float currY;
                int action = event.getAction();
                Log.e(TAG, "onTouch: " + String.valueOf(level));
                Log.e(TAG, "onTouch: " + String.valueOf(clevel));
                switch (action) {
                    case MotionEvent.ACTION_DOWN: {
                        mPrevY = event.getRawY();
                    }
                    case MotionEvent.ACTION_MOVE:
                    {
                        currY = event.getRawY();
                        clevel =(int)(mPrevY-currY)/GAP;
                        switch (clevel){
                            case -5:
                                changeImage(v,level-5);
                                break;
                            case -4:
                                changeImage(v,level-4);
                                break;
                            case -3:
                                changeImage(v,level-3);
                                break;
                            case -2:
                                changeImage(v,level-2);
                                break;
                            case -1:
                                changeImage(v,level+-1);
                                break;
                            case 0:
                                changeImage(v,level);
                                break;
                            case 1:
                                changeImage(v,level+1);
                                break;
                            case 2:
                                changeImage(v,level+2);
                                break;
                            case 3:
                                changeImage(v,level+3);
                                break;
                            case 4:
                                changeImage(v,level+4);
                                break;
                            case 5:
                                changeImage(v,level+5);
                                break;
                        }
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL:
                        break;

                    case MotionEvent.ACTION_UP:
                        level += clevel;
                        if (level>5) level = 5;
                        if (level<0) level = 0;
                        priceGen = level;
                        break;
                }
                return true;
            }
        });
        return view;
    }


    private void loadNearByPlaces(double latitude, double longitude) {
        Log.e(TAG, "loadNearByPlaces: ");
        String type = "food";
        StringBuilder googlePlacesUrl =
                new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=").append(latitude).append(",").append(longitude);
        googlePlacesUrl.append("&radius=").append(PROXIMITY_RADIUS);
        googlePlacesUrl.append("&types=").append(type);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + GOOGLE_BROWSER_API_KEY);

        JsonObjectRequest request = new JsonObjectRequest(googlePlacesUrl.toString(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject result) {

                        Log.i(AppConfig.TAG, "onResponse: Result= " + result.toString());
                        parseLocationResult(result);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(AppConfig.TAG, "onErrorResponse: Error= " + error);
                        Log.e(AppConfig.TAG, "onErrorResponse: Error= " + error.getMessage());
                    }
                });

        AppController.getInstance().addToRequestQueue(request);
    }

    ArrayList<Double> lats = new ArrayList<>();
    ArrayList<Double> lons = new ArrayList<>();

    private void parseLocationResult(JSONObject result) {

        String id, place_id, placeName = null, reference, icon, vicinity = null;
        double latitude, longitude;
        lats = new ArrayList<>();
        lons = new ArrayList<>();
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        ArrayList<String> name = new ArrayList<String>();
        try {
            JSONArray jsonArray = result.getJSONArray("results");

            if (result.getString(STATUS).equalsIgnoreCase(OK)) {

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject place = jsonArray.getJSONObject(i);
                    id = place.getString(FOOD_ID);
                    place_id = place.getString(PLACE_ID);
                    if (!place.isNull(NAME)) {
                        placeName = place.getString(NAME);
                        name.add(placeName);
                    }
                    if (!place.isNull(VICINITY)) {
                        vicinity = place.getString(VICINITY);
                    }
                    latitude = place.getJSONObject(GEOMETRY).getJSONObject(LOCATION)
                            .getDouble(LATITUDE);
                    lats.add(latitude);
                    longitude = place.getJSONObject(GEOMETRY).getJSONObject(LOCATION)
                            .getDouble(LONGITUDE);
                    lons.add(longitude);
                }

                name.add("Other");
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, name);
                spinner.setAdapter(adapter);
                PhotoPreview.lat = lats.get(0);
                PhotoPreview.lon = lons.get(0);
                Log.e(TAG, adapter.getItem(0));
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view2, int position, long id) {
                        EditText editText = (EditText) view.findViewById(R.id.customLocationEditText);
                        if (parent.getItemAtPosition(position)=="Other"){
                            editText.setText("");
                            editText.setVisibility(View.VISIBLE);
                            editText.setActivated(true);
                            PhotoPreview.lat = pLastLocation.getLatitude();
                            PhotoPreview.lon = pLastLocation.getLongitude();
                        }
                        else{
                            editText.setVisibility(View.INVISIBLE);
                            editText.setActivated(false);
                            PhotoPreview.lat = lats.get(position);
                            PhotoPreview.lon = lons.get(position);
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent){
                    }
                });
            } else if (result.getString(STATUS).equalsIgnoreCase(ZERO_RESULTS)) {
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(AppConfig.TAG, "parseLocationResult: Error=" + e.getMessage());
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e(TAG, "onAttach: ");
        mActivity = (Activity) context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onMunched(String comment, float quality, float price, float generousity, String location,double lat,double lon, boolean more);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mGoogleApiClient.disconnect();
        mListener = null;
    }

    private void setPic() {
        mImageView.setImageBitmap(ImageHelper.decodeSampledBitmapFromResource(mParam1, 200, 200));
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
    }

    static Location pLastLocation;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(mActivity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mActivity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation!=null) {
            pLastLocation = mLastLocation;
            loadNearByPlaces(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        }else{
            loadNearByPlaces(pLastLocation.getLatitude(), pLastLocation.getLongitude());
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
