package com.example.thamt.munchat;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.w3c.dom.Text;

import java.util.HashSet;
import java.util.Set;

import static android.content.Context.LOCATION_SERVICE;
import static com.google.android.gms.wearable.DataMap.TAG;

public class NewsFragment extends Fragment implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, UpdatableFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static LocationManager locationManager;
    public static GoogleApiClient mGoogleApiClient;
    private Activity mActivity;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public NewsFragment() {
        // Required empty public constructor
    }

    public static NewsFragment newInstance(String param1, String param2) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        locationManager = (LocationManager) mActivity.getSystemService(LOCATION_SERVICE);
        mGoogleApiClient.connect();
    }

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_news, container, false);
        shown = new HashSet<>();
        nearby = new HashSet<>();
        nearbys = new ArrayMap<>();
        new PopulateProfile().execute();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mGoogleApiClient.disconnect();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String path, String comment, String quality, String price, String generousity, String location, String uploader, String lat, String lon, String fav);
    }

    public void update() {
        new PopulateProfile().execute();
    }

    Set<String> shown;
    Set<String> nearby;
    ArrayMap<String, FrameLayout> nearbys;
    double clat = 0, clon = 0;

    private class PopulateProfile extends AsyncTask<String, String, String> {
        LinearLayout linearRecent = (LinearLayout) view.findViewById(R.id.recent_linearLayour);
        LinearLayout linearFavourite = (LinearLayout) view.findViewById(R.id.favourite_linearLayour);
        LinearLayout linearNearby = (LinearLayout) view.findViewById(R.id.nearby_linearLayour);

        @Override
        protected String doInBackground(String... params) {
            DBHelper mydb = new DBHelper(mActivity);
            try {
                int totalRows = mydb.numberOfRows();
                int i = 0;
                if (mGoogleApiClient.isConnected()) {
                    if (ActivityCompat.checkSelfPermission(mActivity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mActivity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        clat = pLastLocation.getLatitude();
                        clon = pLastLocation.getLongitude();
                    } else {
                        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (mLastLocation != null) {
                            clat = mLastLocation.getLatitude();
                            clon = mLastLocation.getLongitude();
                            pLastLocation = mLastLocation;
                        } else {
                            clat = pLastLocation.getLatitude();
                            clon = pLastLocation.getLongitude();
                        }
                    }
                }
                for (String path : mydb.getAllImages()) {
                    if (i < totalRows - 30) continue;
                    Cursor cur;
                    cur = mydb.getData(path);
                    cur.moveToFirst();
                    String uploader = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_UPLOADER));
                    String imageComment = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_COMMENT));
                    String imageP = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_RatingP));
                    String imageQ = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_RatingQ));
                    String imageG = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_RatingG));
                    String location = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_LOCATION));
                    String lat = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_LAT));
                    String lon = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_LON));
                    String fav = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_Fav));
                    double llat = Double.valueOf(lat), llon = Double.valueOf(lon);
                    double latdistance = Math.abs(llat - clat) * 110946.2172;
                    double londistance = Math.abs(llon - clon) * 111320.5903 * Math.cos(llat + clat) / 2;
                    double distance = Math.sqrt(latdistance * latdistance + londistance * londistance);
                    if (distance < 500 && fav.compareTo("1")==0) {
                        if (nearby.contains(path) == false) {
                            publishProgress(path, imageComment, imageQ, imageP, imageG, location, String.valueOf(0), String.valueOf((int) distance), lat, lon, fav);
                            nearby.add(path);
                        } else {
                            publishProgress(path, imageComment, imageQ, imageP, imageG, location, String.valueOf(4), String.valueOf((int) distance), lat, lon, fav);
                        }
                    } else {
                        if (nearby.contains(path)) {
                            //Remove from display
                            nearby.remove(path);
                            publishProgress(path, imageComment, imageQ, imageP, imageG, location, "3", String.valueOf(distance));
                        }
                    }
                    if (shown.contains(path) == false) {
                        shown.add(path);
                        if (fav.compareTo("1") == 0)
                            publishProgress(path, imageComment, imageQ, imageP, imageG, location, String.valueOf(2), uploader, lat, lon, fav);
                        else {
                            if (uploader.compareTo(MainActivity.MyID) == 0) continue;
                            i++;
                            publishProgress(path, imageComment, imageQ, imageP, imageG, location, String.valueOf(1), uploader, lat, lon, fav);
                        }
                    }
                }
            } catch (Exception e){
                Log.e("NewsError",e.toString());
            }
        mydb.close();
        return null;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        private void setPic(ImageView mImageView, String imagePath) {
            mImageView.setImageBitmap(ImageHelper.decodeSampledBitmapFromResource(imagePath, 200, 200));
        }

        @Override
        protected void onProgressUpdate(final String... values) {
            FrameLayout frame = new FrameLayout(mActivity);
            frame.setPadding(0,0,0,0);

            ImageView mImageView = new ImageView(mActivity);
            mImageView.setAdjustViewBounds(true);
            mImageView.setPadding(0, 0, 0, 0);
            mImageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    mListener.onFragmentInteraction(values[0], values[1], values[2], values[3], values[4], values[5], values[7], values[8], values[9], values[10]);
                }
            });
            setPic(mImageView, values[0]);
            switch (values[6]) {
                case "1":
                    linearRecent.addView(mImageView);
                    break;
                case "2":
                    linearFavourite.addView(mImageView);
                    break;
                case "4":
                    FrameLayout frame2 = nearbys.get(values[0]);
                    TextView tv2 = (TextView) frame2.findViewWithTag("5");
                    tv2.setText(values[7]);
                    break;
                case "0":
                    frame.addView(mImageView);
                    TextView tv = new TextView(mActivity);
                    tv.setText(values[7]);
                    tv.setTextColor(0xFFFFFFFF);
                    tv.setBackgroundColor(0xFF000000);
                    tv.setTag("5");
                    frame.addView(tv);
                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.gravity = Gravity.RIGHT;
                    tv.setLayoutParams(params);
                    linearNearby.addView(frame);
                    nearbys.put(values[0], frame);
                    break;
                default:
                    linearNearby.removeView(nearbys.get(values[0]));
                    nearbys.remove(values[0]);
                    break;
            }
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {
    }

    static Location pLastLocation;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(mActivity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mActivity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startLocationUpdates();
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            pLastLocation = mLastLocation;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {
        pLastLocation = location;
        this.update();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected())
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    protected void startLocationUpdates() {
        LocationRequest mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5000)
                .setFastestInterval(1000);

        if (ActivityCompat.checkSelfPermission(mActivity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mActivity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }
    @Override
    public void onResume() {
        super.onResume();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mGoogleApiClient.isConnected()) {
                    startLocationUpdates();
                }
            }
        }, 2000);
    }
}
