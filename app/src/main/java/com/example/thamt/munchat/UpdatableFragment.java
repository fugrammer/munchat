package com.example.thamt.munchat;

/**
 * Created by thamt on 18/10/2016.
 */

public interface UpdatableFragment {
    public void update();
}
