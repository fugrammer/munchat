package com.example.thamt.munchat;

/**
 * Created by thamt on 11/10/2016.
 */

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.*;

@DynamoDBTable(tableName = "MainTable2")
public class DataMap {

    private String userID;
    private long time;
    private String comment;
    private float price;
    private float quality;
    private float generousity;
    private String location;
    private String image;
    private String upLoader;
    private double lat;
    private double lon;

    @DynamoDBHashKey(attributeName = "UserID")
    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    @DynamoDBRangeKey(attributeName = "Time")
    public long getTime() {
        return this.time;
    }

    public void setTime(long time) {
        this.time = time;
    }
    @DynamoDBAttribute(attributeName = "Lat")
    public double getLat() {
        return this.lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
    @DynamoDBAttribute(attributeName = "Lon")
    public double getLon() {
        return this.lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
    @DynamoDBAttribute(attributeName = "Price")
    public float getPrice() {
        return this.price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @DynamoDBAttribute(attributeName = "Quality")
    public float getQuality() {
        return this.quality;
    }
    public void setQuality(float quality) {
        this.quality = quality;
    }

    @DynamoDBAttribute(attributeName = "Generousity")
    public float getGenerousity() {
        return this.generousity;
    }
    public void setGenerousity(float generousity) {
        this.generousity = generousity;
    }

    @DynamoDBAttribute(attributeName = "Comment")
    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @DynamoDBAttribute(attributeName = "Image")
    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @DynamoDBAttribute(attributeName = "Location")
    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @DynamoDBAttribute(attributeName = "Uploader")
    public String getUploader() {
        return this.upLoader;
    }

    public void setUploader(String uploader) {
        this.upLoader = uploader;
    }
}