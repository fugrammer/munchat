package com.example.thamt.munchat;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ProfileFragment extends Fragment implements UpdatableFragment{

    private static final String ARG_PARAM1 = "userID";

    // TODO: Rename and change types of parameters
    private String mParam1;
    View view;
    private OnFragmentInteractionListener mListener;

    public ProfileFragment() {
        // Required empty public constructor
    }
    static Set<String> shown;
    public static ProfileFragment newInstance(String param1) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }
    static int left = 1;
    private class PopulateProfile extends AsyncTask<String, String, String> {
        LinearLayout linearLeft = (LinearLayout) view.findViewById(R.id.linearLeft);
        LinearLayout linearRight = (LinearLayout) view.findViewById(R.id.linearRight);
        @Override
        protected String doInBackground(String... params) {
            DBHelper mydb = new DBHelper(getActivity());
            for (String path: mydb.getAllImages()){
                Cursor cur;
                cur = mydb.getData(path);
                cur.moveToFirst();
                String uploader = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_UPLOADER));
                if (uploader.compareTo(mParam1)!=0) continue;
                String imageComment=cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_COMMENT));
                String imageP=cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_RatingP));
                String imageQ=cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_RatingQ));
                String imageG=cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_RatingG));
                String location=cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_LOCATION));
                //String uploader = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_UPLOADER));
                String lat = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_LAT));
                String lon = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_LON));
                String fav = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_Fav));

                if (shown.contains(path)==false){
                    shown.add(path);
                    publishProgress(path,imageComment,String.valueOf(left),imageQ,imageP,imageG,location,uploader,lat,lon,fav);
                    if (left==1) {
                        left = 0;
                    }else{
                        left=1;
                    }
                }

            }
            mydb.close();
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(final String... values) {
            ImageView mImageView = new ImageView(getActivity());
            mImageView.setAdjustViewBounds(true);
            mImageView.setPadding(0,0,0,0);
            mImageView.setOnClickListener(new View.OnClickListener(){
                public void onClick (View view){
                    mListener.onFragmentInteraction(values[0],values[1],values[3],values[4],values[5],values[6],values[7],values[8],values[9],values[10]);
                }
            });
            setPic(mImageView,values[0]);
            if (values[2]=="1") {
                linearLeft.addView(mImageView);
            }else{
                linearRight.addView(mImageView);
            }
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        new PopulateProfile().execute();
        shown = new HashSet<>();
        left=1;
        return view;
    }

    public void update(){
        new PopulateProfile().execute();
    }

    private void setPic(ImageView mImageView, String imagePath) {
        mImageView.setImageBitmap(ImageHelper.decodeSampledBitmapFromResource(imagePath,200,200));
    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String path, String comment ,String quality, String price, String generousity,String location,String uploader,String lat, String lon, String fav);
    }
}
