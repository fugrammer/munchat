package com.example.thamt.munchat;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Set;


public class FriendFragment extends Fragment implements UpdatableFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    View view;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FriendFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FriendFragment newInstance(String param1, String param2) {
        FriendFragment fragment = new FriendFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_friend, container, false);
        new PopulateProfile().execute();
        shown = new HashSet<>();
        left = 1;
        return view;
    }

    public void update(){
        new PopulateProfile().execute();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    static int left = 1;
    static Set<String> shown;
    private class PopulateProfile extends AsyncTask<String, String, String> {
        LinearLayout linearLeft = (LinearLayout) view.findViewById(R.id.linearFLeft);
        LinearLayout linearRight = (LinearLayout) view.findViewById(R.id.linearFRight);
        LinearLayout linearMid = (LinearLayout) view.findViewById(R.id.linearFMid);

        @Override
        protected String doInBackground(String... params) {
            DBHelper mydb = new DBHelper(getActivity());
            Set<String> uploaders = new HashSet<>();

            for (String path: mydb.getAllImages()) {
                Cursor cur;
                cur = mydb.getData(path);
                cur.moveToFirst();
                String uploader = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_UPLOADER));
                if (uploader.compareTo(MainActivity.MyID) != 0) {
                    uploaders.add(uploader);
                }
            }
            for (String uploader:uploaders){
                if (shown.contains(uploader)==false) {
                    shown.add(uploader);
                    publishProgress(uploader, String.valueOf(left));
                    if (left == 1) {
                        left = 2;
                    } else if (left == 2) {
                        left = 3;
                    } else {
                        left = 1;
                    }
                }
            }
            mydb.close();
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(final String... values) {
            ImageView mImageView = new ImageView(getActivity());
            mImageView.setAdjustViewBounds(true);
            mImageView.setPadding(16,16,16,0);
            mImageView.setOnClickListener(new View.OnClickListener(){
                public void onClick (View view){
                    mListener.onFragmentInteraction(values[0]);
                }
            });
            TextView tv = new TextView(getActivity());
            tv.setText(values[0]);
            tv.setPadding(16,0,16,16);
            tv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT,0));
            mImageView.setImageResource(R.drawable.ic_menu_camera);
            if (values[1]=="1") {
                linearLeft.addView(mImageView);
                linearLeft.addView(tv);
            }else if (values[1]=="2"){
                linearMid.addView(mImageView);
                linearMid.addView(tv);
            }
            else{
                linearRight.addView(mImageView);
                linearRight.addView(tv);
            }
        }
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String uploader);
    }
}
