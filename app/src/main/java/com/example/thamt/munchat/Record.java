package com.example.thamt.munchat;

import android.location.Location;

/**
 * Created by thamt on 11/10/2016.
 */

public class Record {
    private String image;
    private String comment;
    private float price;
    private float quality;
    private float generousity;
    private String location;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    private long time;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    private double lat;
    private double lon;

    public String getUpLoader() {
        return upLoader;
    }

    public void setUpLoader(String upLoader) {
        this.upLoader = upLoader;
    }

    private String upLoader;

    public Record(String image, String comment, float price, float quality, float generousity, String location,String upLoader) {
        this.image = image;
        this.comment = comment;
        this.price = price;
        this.quality = quality;
        this.generousity = generousity;
        this.location = location;
        this.upLoader = upLoader;
    }

    public Record(String image, String comment, float price, float quality, float generousity, String location,String upLoader,double lat,double lon) {
        this.image = image;
        this.comment = comment;
        this.price = price;
        this.quality = quality;
        this.generousity = generousity;
        this.location = location;
        this.upLoader = upLoader;
        this.lon=lon;
        this.lat=lat;
    }
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getQuality() {
        return quality;
    }

    public void setQuality(float quality) {
        this.quality = quality;
    }

    public float getGenerousity() {
        return generousity;
    }

    public void setGenerousity(float generousity) {
        this.generousity = generousity;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


}
