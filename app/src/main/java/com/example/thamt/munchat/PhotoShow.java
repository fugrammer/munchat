package com.example.thamt.munchat;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;

public class PhotoShow extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "imageDetails";

    // TODO: Rename and change types of parameters
    private ArrayList<String> mParam1;

    private OnFragmentInteractionListener mListener;

    public PhotoShow() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PhotoShow newInstance(String param1) {
        PhotoShow fragment = new PhotoShow();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    ImageView mImageView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photo_show, container, false);
        if (getArguments() != null) {
            mParam1 = getArguments().getStringArrayList(ARG_PARAM1);
        }
        DBHelper mydb = new DBHelper(getActivity());
        Cursor cur = mydb.getData(mParam1.get(0));
        cur.moveToFirst();
        String fav = cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_Fav));
        String imageComment=cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_COMMENT));
        String imageP=cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_RatingP));
        String imageQ=cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_RatingQ));
        String imageG=cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_RatingG));
        String location2=cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_LOCATION));
        String uploader=cur.getString(cur.getColumnIndex(mydb.COLUMN_IMAGE_UPLOADER));

        cur.close();
        mydb.close();

        File imgFile = new  File(mParam1.get(0));
        mImageView = (ImageView) view.findViewById(R.id.imageView2);
        setPic();
        TextView tv1 = (TextView) view.findViewById(R.id.photoShowComment);
        tv1.setText(imageComment);
        float ratingQ = Float.valueOf(imageQ);
        RatingBar rb1 = (RatingBar) view.findViewById(R.id.showRatingBarQ);
        rb1.setRating(ratingQ);
        float ratingP = Float.valueOf(imageP);
        RatingBar rb2 = (RatingBar) view.findViewById(R.id.showRatingBarP);
        rb2.setRating(ratingP);
        float ratingG = Float.valueOf(imageG);
        RatingBar rb3 = (RatingBar) view.findViewById(R.id.showRatingBarG);
        rb3.setRating(ratingG);
        TextView tv = (TextView) view.findViewById(R.id.locationTextView);
        tv.setText(location2);
        Button okButton = (Button) view.findViewById(R.id.photoShowOK);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        CheckBox favCheckBox = (CheckBox) view.findViewById(R.id.FavouriteCheckBox);
        if (fav.compareTo("1")==0) favCheckBox.setChecked(true);
        else favCheckBox.setChecked(false);
        favCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked2 = ((CheckBox) v).isChecked();
                String checked;
                if (checked2) checked = "1";
                else checked = "0";
                if (mListener!=null){
                    mListener.onFavourite(mParam1.get(0),mParam1.get(1),Float.valueOf(mParam1.get(2)),Float.valueOf(mParam1.get(3)),Float.valueOf(mParam1.get(4)),mParam1.get(5),mParam1.get(6),Double.valueOf(mParam1.get(7)),Double.valueOf(mParam1.get(8)),checked);
                }
            }
        });
        ImageButton deleteButton = (ImageButton) view.findViewById(R.id.deleteButton);
        if (uploader.compareTo(MainActivity.MyID)!=0){
            deleteButton.setActivated(false);
            deleteButton.setVisibility(View.INVISIBLE);
        }
        else {
            deleteButton.setVisibility(View.VISIBLE);
            deleteButton.setActivated(true);
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onDelete(mParam1.get(0), mParam1.get(1), Float.valueOf(mParam1.get(2)), Float.valueOf(mParam1.get(3)), Float.valueOf(mParam1.get(4)), mParam1.get(5), mParam1.get(6), Double.valueOf(mParam1.get(7)), Double.valueOf(mParam1.get(8)));
                    }
                }
            });
        }
        return view;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFavourite(String name, String comment,float ratingQ, float ratingP, float ratingG,String location,String uploader, double lat, double lon,String fav);
        public void onDelete (String name, String comment,float ratingQ, float ratingP, float ratingG,String location,String uploader, double lat, double lon);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setPic() {
        mImageView.setImageBitmap(ImageHelper.decodeSampledBitmapFromResource(mParam1.get(0),200,200));
    }
}
